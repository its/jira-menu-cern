RELEASE = $(shell date +'%d-%m-%Y at %H:%M')

all:
	sed -e "s#@VERSION@#$(RELEASE)#" pom.xml.template >pom.xml
	atlas-mvn package
	rm pom.xml

doc:
	sed -e "s#@VERSION@#$(RELEASE)#" pom.xml.template >pom.xml
	atlas-mvn javadoc:javadoc
	rm pom.xml
