# JIRA menu CERN

Codeless plugin to add menu item to JIRA's main page. The file that does all the work is [atlassian-plugin.xml](src/main/resources/atlassian-plugin.xml)
