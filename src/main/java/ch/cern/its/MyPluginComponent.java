package ch.cern.its;

public interface MyPluginComponent
{
    String getName();
}
